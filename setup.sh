#!/usr/bin/env bash

# Install proto
PROTOC_ZIP=protoc-3.3.0-linux-x86_64.zip
curl -OL https://github.com/google/protobuf/releases/download/v3.3.0/${PROTOC_ZIP}
sudo unzip -o ${PROTOC_ZIP} -d /usr/local bin/protoc
rm -f ${PROTOC_ZIP}

# Install Java, Maven
sudo apt-get update
sudo apt-get upgrade -y
sudo apt-get install -y software-properties-common
yes "" | sudo add-apt-repository ppa:linuxuprising/java
sudo apt-get update
sudo apt-get install -y oracle-java11-installer

sudo apt-get -y install maven
mvn -version
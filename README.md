# Protobuff --> File using Java

The project consists of simple http server which accepts JSON data as POST request, creates protobuff from it and stores it in a file
in threadsafe and rolls over after timeout using log4j2 library.

#### Steps to install, run, test

1. Clone the project using `git clone https://kishan03@bitbucket.org/kishan03/protobuf_java.git`. 

2. `cd` to project directory.

3. Install: `./setup.sh`.

4. Start server: `./start_server.sh`.

5. Make POST request: `curl -X POST \
                        http://localhost:8080/user/ \
                        -H 'Content-Type: application/json' \
                        -d '{"name": "kishan", "id": 26}'`.

6. If request successful protobuf will be saved in `datafiles/persons.log` folder. 

7. Tests: `mvn test`.
package constants;

import org.junit.Test;

import static constants.Constants.CWD;
import static constants.Constants.TIMEOUT;
import static org.junit.Assert.assertEquals;

public class ConstantsTest {
    @Test
    public void testConstantValues() {
        String current_directory = System.getProperty("user.dir");
        assertEquals(current_directory, CWD);
        assertEquals(300000, TIMEOUT);
    }

}
package userhandler;

import org.junit.Test;
import proto.PersonProto;

import static org.junit.Assert.assertEquals;
import static userhandler.CreatePerson.createPerson;


public class CreatePersonTest {

    @Test
    public void createPersonTest() {
        PersonProto.Person.Builder person = PersonProto.Person.newBuilder();
        person.setName("test");
        person.setId(1);
        assertEquals(person.build(), createPerson(1, "test"));
    }
}
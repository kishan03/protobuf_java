package constants;

public class Constants {
    private Constants() {
    }

    public static int TIMEOUT = 300000; // 5 minutes
    public static String CWD = System.getProperty("user.dir");
}

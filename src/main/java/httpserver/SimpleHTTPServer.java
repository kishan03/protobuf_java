package httpserver;

import com.sun.net.httpserver.HttpServer;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import userhandler.PersonHandler;

import java.io.IOException;
import java.net.InetSocketAddress;

/**
 * A simple http server using plain java.
 *
 * @author Kishan
 * @version 1.0
 */
public class SimpleHTTPServer {
    private static final Logger logger = LogManager.getLogger(SimpleHTTPServer.class);

    public static void main(String[] args) {
        try {
            System.out.println("Listening at localhost:8080...");
            HttpServer httpServer = HttpServer.create(new InetSocketAddress(8080), 0);
            httpServer.createContext("/user", new PersonHandler());
            httpServer.start();
        } catch (IOException ex) {
            logger.error("Error starting a server: ", ex.toString());
        }
    }
}
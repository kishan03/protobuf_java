package userhandler;

import proto.PersonProto;

public class CreatePerson {
    // This function fills in a Person message based on user input.
    public static PersonProto.Person createPerson(int id,
                                                  String name) {
        PersonProto.Person.Builder person = PersonProto.Person.newBuilder();
        person.setId(id);
        person.setName(name);
        return person.build();
    }
}
package userhandler;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.json.JSONObject;
import writemessage.WriteMessage;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;


/**
 * A person handler that serves POST requests and
 * writes the protobuff to a file.
 *
 * @author Kishan
 * @version 1.0
 */
public class PersonHandler implements HttpHandler {
    /**
     * Accepts POST request data and creates a protobuff based on it.
     * Protobuff is then stored to a file using threadsafe method.
     *
     * @param httpExchange
     */

    private static final Logger logger = LogManager.getLogger(PersonHandler.class);

    @Override
    public void handle(HttpExchange httpExchange) {

        System.out.println("Serving the request");

        // Serve for POST requests only
        if (httpExchange.getRequestMethod().equalsIgnoreCase("POST")) {

            try {

                Headers requestHeaders = httpExchange.getRequestHeaders();
                int contentLength = Integer.parseInt(requestHeaders.getFirst("Content-length"));

                // Converting request body to JSON object
                InputStream inputStream = httpExchange.getRequestBody();
                byte[] data = new byte[contentLength];
                int length = inputStream.read(data);
                JSONObject object = new JSONObject(new String(data));

                // Threadsafe writing to file
                boolean wrote = WriteMessage.getInstance().writeToFile(object.getInt("id"), object.getString("name"));
                String response;
                if (wrote) {
                    response = "Message written to file successfully!";
                } else {
                    response = "Error writing message to file!";
                }
                httpExchange.sendResponseHeaders(HttpURLConnection.HTTP_OK, response.length());
                OutputStream os = httpExchange.getResponseBody();
                os.write(response.getBytes());

                os.close();
                httpExchange.close();

            } catch (Exception e) {
                System.out.println(e.toString());
                logger.error("Error while handling POST request on /user endpoint: ", e.toString());
            }
        }

    }
}


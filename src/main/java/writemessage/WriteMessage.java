package writemessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import proto.PersonProto;
import userhandler.CreatePerson;

public class WriteMessage {
    private static final Logger logger = LogManager.getLogger(WriteMessage.class);
    private static final WriteMessage inst = new WriteMessage();

    private WriteMessage() {
        super();
    }

    public synchronized boolean writeToFile(int id, String name) {
        try {
            PersonProto.PersonList.Builder personList = PersonProto.PersonList.newBuilder();
            personList.addPeople(CreatePerson.createPerson(id, name));
            logger.info(personList.build());
            return true;
        } catch (Exception ex) {
            System.out.println(ex.toString());
            return false;
        }


    }

    public static WriteMessage getInstance() {
        return inst;
    }

}